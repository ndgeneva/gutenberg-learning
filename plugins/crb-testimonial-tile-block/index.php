<?php
/**
 * Plugin Name: CRB Testimonial Tile Block
 * Version: 1.0.0
 */

add_action( 'enqueue_block_editor_assets', 'crb_load_testimonial_tile_block_assets' );
function crb_load_testimonial_tile_block_assets() {
	wp_enqueue_script(
		'crb-testimonial-tile-block',
		plugins_url( 'block.build.js', __FILE__ ),
		array('wp-blocks','wp-editor'),
		true
	);

	wp_enqueue_style(
		'crb-testimonial-tile-block-editor', // Handle.
		plugins_url( 'editor.css', __FILE__ ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		true
	);
}

add_action( 'enqueue_block_assets', 'crb_load_testimonial_tile_block_assets_frontend' );
function crb_load_testimonial_tile_block_assets_frontend() {
	wp_enqueue_style(
		'crb-testimonial-tile-block-frontend', // Handle.
		plugins_url( 'style.css', __FILE__ ), // Block frontend CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		true
	);
} // End function gb_block_02_basic_esnext_block_assets().