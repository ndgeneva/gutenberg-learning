const { RichText, MediaUpload, PlainText } = wp.editor;
const { registerBlockType } = wp.blocks;
const { Button } = wp.components;

registerBlockType('crb/testimonial-tile', {
  title: 'CRB Testimonial Tile Block',
  icon: 'screenoptions',
  category: 'common',
  attributes: {
    iconURL: {
      attribute: 'src',
      selector: '.tile__image'
    },
    title: {
      source: 'text',
      selector: '.tile__title'
    },
    content: {
      type: 'array',
      source: 'children',
      selector: '.tile__content-wrapper'
    },
  },

  edit({ attributes, className, setAttributes }) {
    const getImageButton = (openEvent) => {
      if(attributes.iconURL) {
        return (
          <div class="tile__image">
            <img
              src={ attributes.iconURL }
              onClick={ openEvent }
            />
          </div>
        );
      }
      else {
        return (
          <div className="button-container">
            <Button
              onClick={ openEvent }
              className="button button-large"
            >
              Pick an image
            </Button>
          </div>
        );
      }
    };

    return (
      <div className="container">
        <div className="tile-testimonial">
          <MediaUpload
            onSelect={ media => { setAttributes({ iconURL: media.url }); } }
            type="image"
            value={ attributes.imageID }
            render={ ({ open }) => getImageButton(open) }
          />
          <div className="tile__content">
            <PlainText
              onChange={ content => setAttributes({ title: content }) }
              value={ attributes.title }
              placeholder="Testimonial Title"
              className="tile__title"
            />
            <RichText
              onChange={ content => setAttributes({ content: content }) }
              value={ attributes.content }
              multiline="p"
              placeholder="Testimonial Content"
              className="tile__content-wrapper"
            />
          </div>
        </div>
      </div>
    );
  },

  save({ attributes }) {
    return (
      <div className="wp-block-crb-testimonial-tile-block tile-testimonial">
        <div class="tile__image" style={{ backgroundImage: "url(" + attributes.iconURL + ")"}}></div>

        <div class="tile__content">
          <h3 className="tile__title">{ attributes.title }</h3>

          <div className="tile__content-wrapper">
            { attributes.content }
          </div>
        </div>
      </div>
    );
  }
});