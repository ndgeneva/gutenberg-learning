/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var _wp$editor = wp.editor,
    RichText = _wp$editor.RichText,
    MediaUpload = _wp$editor.MediaUpload,
    PlainText = _wp$editor.PlainText;
var registerBlockType = wp.blocks.registerBlockType;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    ColorPicker = _wp$components.ColorPicker;


registerBlockType('crb/slider', {
  title: 'CRB Slider Block - TODO',
  icon: 'screenoptions',
  category: 'common',
  attributes: {
    color: {
      selector: '.tile-feature'
    },
    iconURL: {
      attribute: 'src',
      selector: '.tile__icon img'
    },
    title: {
      source: 'text',
      selector: '.tile__title'
    },
    content: {
      type: 'array',
      source: 'children',
      selector: '.tile__content-wrapper'
    }
  },

  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        className = _ref.className,
        setAttributes = _ref.setAttributes;

    var getImageButton = function getImageButton(openEvent) {
      if (attributes.iconURL) {
        return wp.element.createElement(
          'div',
          { 'class': 'tile__icon', style: { backgroundColor: attributes.color } },
          wp.element.createElement('img', {
            src: attributes.iconURL,
            onClick: openEvent
          })
        );
      } else {
        return wp.element.createElement(
          'div',
          { className: 'button-container', style: { backgroundColor: attributes.color } },
          wp.element.createElement(
            Button,
            {
              onClick: openEvent,
              className: 'button button-large'
            },
            'Pick an image'
          )
        );
      }
    };

    return wp.element.createElement(
      'div',
      { className: 'container' },
      wp.element.createElement(ColorPicker, {
        color: attributes.color,
        onChangeComplete: function onChangeComplete(value) {
          return setAttributes({ color: value.hex });
        },
        disableAlpha: true
      }),
      wp.element.createElement(
        'div',
        { className: 'tile-feature', style: { border: "3px solid " + attributes.color } },
        wp.element.createElement(MediaUpload, {
          onSelect: function onSelect(media) {
            setAttributes({ iconURL: media.url });
          },
          type: 'image',
          value: attributes.imageID,
          render: function render(_ref2) {
            var open = _ref2.open;
            return getImageButton(open);
          }
        }),
        wp.element.createElement(
          'div',
          { className: 'tile__content' },
          wp.element.createElement(PlainText, {
            onChange: function onChange(content) {
              return setAttributes({ title: content });
            },
            value: attributes.title,
            placeholder: 'Feature Title',
            className: 'tile__title'
          }),
          wp.element.createElement(RichText, {
            onChange: function onChange(content) {
              return setAttributes({ content: content });
            },
            value: attributes.content,
            multiline: 'p',
            placeholder: 'Feature Content',
            className: 'tile__content-wrapper'
          })
        )
      )
    );
  },
  save: function save(_ref3) {
    var attributes = _ref3.attributes;

    return wp.element.createElement(
      'div',
      { className: 'wp-block-crb-feature-tile-block tile-feature', style: { border: "3px solid " + attributes.color } },
      wp.element.createElement(
        'div',
        { 'class': 'tile__icon', style: { backgroundColor: attributes.color } },
        wp.element.createElement('img', { src: attributes.iconURL })
      ),
      wp.element.createElement(
        'div',
        { 'class': 'tile__content' },
        wp.element.createElement(
          'h3',
          { className: 'tile__title' },
          attributes.title
        ),
        wp.element.createElement(
          'div',
          { className: 'tile__content-wrapper' },
          attributes.content
        )
      )
    );
  }
});

/***/ })
/******/ ]);