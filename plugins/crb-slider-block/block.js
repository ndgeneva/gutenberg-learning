const { RichText, MediaUpload, PlainText } = wp.editor;
const { registerBlockType } = wp.blocks;
const { Button, ColorPicker } = wp.components;

registerBlockType('crb/slider', {
  title: 'CRB Slider Block - TODO',
  icon: 'screenoptions',
  category: 'common',
  attributes: {
    color: {
      selector: '.tile-feature'
    },
    iconURL: {
      attribute: 'src',
      selector: '.tile__icon img'
    },
    title: {
      source: 'text',
      selector: '.tile__title'
    },
    content: {
      type: 'array',
      source: 'children',
      selector: '.tile__content-wrapper'
    },
  },

  edit({ attributes, className, setAttributes }) {
    const getImageButton = (openEvent) => {
      if(attributes.iconURL) {
        return (
          <div class="tile__icon" style={{ backgroundColor: attributes.color }}>
            <img
              src={ attributes.iconURL }
              onClick={ openEvent }
            />
          </div>
        );
      }
      else {
        return (
          <div className="button-container" style={{ backgroundColor: attributes.color }}>
            <Button
              onClick={ openEvent }
              className="button button-large"
            >
              Pick an image
            </Button>
          </div>
        );
      }
    };

    return (
      <div className="container">
        <ColorPicker
          color={ attributes.color }
          onChangeComplete={ ( value ) => setAttributes({ color: value.hex }) }
          disableAlpha
        />

        <div className="tile-feature" style={{ border: "3px solid " + attributes.color }}>
          <MediaUpload
            onSelect={ media => { setAttributes({ iconURL: media.url }); } }
            type="image"
            value={ attributes.imageID }
            render={ ({ open }) => getImageButton(open) }
          />
          <div className="tile__content">
            <PlainText
              onChange={ content => setAttributes({ title: content }) }
              value={ attributes.title }
              placeholder="Feature Title"
              className="tile__title"
            />
            <RichText
              onChange={ content => setAttributes({ content: content }) }
              value={ attributes.content }
              multiline="p"
              placeholder="Feature Content"
              className="tile__content-wrapper"
            />
          </div>
        </div>
      </div>
    );
  },

  save({ attributes }) {
    return (
      <div className="wp-block-crb-feature-tile-block tile-feature" style={{ border: "3px solid " + attributes.color }}>
        <div class="tile__icon" style={{ backgroundColor: attributes.color }}>
          <img src={attributes.iconURL}/>
        </div>

        <div class="tile__content">
          <h3 className="tile__title">{ attributes.title }</h3>

          <div className="tile__content-wrapper">
            { attributes.content }
          </div>
        </div>
      </div>
    );
  }
});