const { RichText, MediaPlaceholder, PlainText, InspectorControls } = wp.editor;
const { registerBlockType } = wp.blocks;
const { Button, TextControl, ToggleControl, Panel, PanelBody, PanelRow } = wp.components;
const { Fragment } = wp.element;

registerBlockType('crb/video', {
  title: 'CRB Video',
  icon: 'format-video',
  category: 'common',
  supports: {
    align: true,
  },
  attributes: {
    link: {
      attribute: 'href',
      selector: '.video__link'
    },
    imageUrl: {
      attribute: 'src',
      selector: '.video'
    }
  },

  edit({ attributes, className, setAttributes }) {
    const getImageButton = (openEvent) => {
      return (
        <div className="button-container">
          <Button
            onClick={ openEvent }
            className="button button-large"
          >
            Pick an image
          </Button>
        </div>
      );
    };

    return (
      <div className="container">
        <div className="section-video">
          <div className="video" style={{ backgroundImage: "url(" + attributes.imageUrl + ")"}} >
            {attributes.link !== '' &&
              <span className="video__link"></span>
            }
          </div>
        </div>
        <Fragment>
          <InspectorControls>
            <PanelBody>
              <PanelRow>
                <div className="media-upload-container" style={{ backgroundImage: "url(" + attributes.imageUrl + ")"}}>
                  <span className="remove-image" onClick={ event => { event.preventDefault(); setAttributes({ imageUrl: '' }); } }></span>
                  <MediaPlaceholder
                    onSelect={ media => { setAttributes({ imageUrl: media.url }); } }
                    allowedTypes = { [ 'image' ] }
                    multiple = { false }
                    labels = { { title: 'The Image' } }
                  >
                    "extra content"
                  </MediaPlaceholder>
                </div>
              </PanelRow>
              <PanelRow>
                <TextControl
                  label="Link"
                  onChange={ content => setAttributes({ link: content }) }
                  value={ attributes.link }
                />
              </PanelRow>
            </PanelBody>
          </InspectorControls>
        </Fragment>
      </div>

    );
  },

  save({ attributes }) {
    return (
      <div className="wp-block-crb-video section-video section-video--desktop">
        <div className="video" style={{ backgroundImage: "url(" + attributes.imageUrl + ")"}} >
          <a href={ attributes.link } className="video__link" mfp-video=""></a>
        </div>
      </div>
    );
  }
});