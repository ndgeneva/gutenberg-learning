/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var _wp$editor = wp.editor,
    RichText = _wp$editor.RichText,
    MediaPlaceholder = _wp$editor.MediaPlaceholder,
    PlainText = _wp$editor.PlainText,
    InspectorControls = _wp$editor.InspectorControls;
var registerBlockType = wp.blocks.registerBlockType;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    TextControl = _wp$components.TextControl,
    ToggleControl = _wp$components.ToggleControl,
    Panel = _wp$components.Panel,
    PanelBody = _wp$components.PanelBody,
    PanelRow = _wp$components.PanelRow;
var Fragment = wp.element.Fragment;


registerBlockType('crb/video', {
  title: 'CRB Video',
  icon: 'format-video',
  category: 'common',
  supports: {
    align: true
  },
  attributes: {
    link: {
      attribute: 'href',
      selector: '.video__link'
    },
    imageUrl: {
      attribute: 'src',
      selector: '.video'
    }
  },

  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        className = _ref.className,
        setAttributes = _ref.setAttributes;

    var getImageButton = function getImageButton(openEvent) {
      return wp.element.createElement(
        'div',
        { className: 'button-container' },
        wp.element.createElement(
          Button,
          {
            onClick: openEvent,
            className: 'button button-large'
          },
          'Pick an image'
        )
      );
    };

    return wp.element.createElement(
      'div',
      { className: 'container' },
      wp.element.createElement(
        'div',
        { className: 'section-video' },
        wp.element.createElement(
          'div',
          { className: 'video', style: { backgroundImage: "url(" + attributes.imageUrl + ")" } },
          attributes.link !== '' && wp.element.createElement('span', { className: 'video__link' })
        )
      ),
      wp.element.createElement(
        Fragment,
        null,
        wp.element.createElement(
          InspectorControls,
          null,
          wp.element.createElement(
            PanelBody,
            null,
            wp.element.createElement(
              PanelRow,
              null,
              wp.element.createElement(
                'div',
                { className: 'media-upload-container', style: { backgroundImage: "url(" + attributes.imageUrl + ")" } },
                wp.element.createElement('span', { className: 'remove-image', onClick: function onClick(event) {
                    event.preventDefault();setAttributes({ imageUrl: '' });
                  } }),
                wp.element.createElement(
                  MediaPlaceholder,
                  {
                    onSelect: function onSelect(media) {
                      setAttributes({ imageUrl: media.url });
                    },
                    allowedTypes: ['image'],
                    multiple: false,
                    labels: { title: 'The Image' }
                  },
                  '"extra content"'
                )
              )
            ),
            wp.element.createElement(
              PanelRow,
              null,
              wp.element.createElement(TextControl, {
                label: 'Link',
                onChange: function onChange(content) {
                  return setAttributes({ link: content });
                },
                value: attributes.link
              })
            )
          )
        )
      )
    );
  },
  save: function save(_ref2) {
    var attributes = _ref2.attributes;

    return wp.element.createElement(
      'div',
      { className: 'wp-block-crb-video section-video section-video--desktop' },
      wp.element.createElement(
        'div',
        { className: 'video', style: { backgroundImage: "url(" + attributes.imageUrl + ")" } },
        wp.element.createElement('a', { href: attributes.link, className: 'video__link', 'mfp-video': '' })
      )
    );
  }
});

/***/ })
/******/ ]);