const { RichText, MediaUpload, PlainText, InspectorControls } = wp.editor;
const { registerBlockType } = wp.blocks;
const { Button, TextControl, Panel, PanelBody, PanelRow } = wp.components;
const { Fragment } = wp.element;

registerBlockType('crb/product-tile', {
  title: 'CRB Product Tile Block',
  icon: 'products',
  category: 'common',
  attributes: {
    title: {
      source: 'text',
      selector: '.tile__title'
    },
    link: {
      attribute: 'href',
      selector: '.tile__link'
    },
    link_label: {
      source: 'text',
      selector: '.tile__link'
    },
    imageUrl: {
      attribute: 'src',
      selector: '.tile__image'
    }
  },

  edit({ attributes, className, setAttributes }) {
     const getImageButton = (openEvent) => {
      return (
        <div className="button-container">
          <Button
            onClick={ openEvent }
            className="button button-large"
          >
            Pick an image
          </Button>
        </div>
      );
    };

    const renderLink = () => {
      {attributes.link}
      if ( attributes.link && attributes.link_label ) {
        return  <span class="tile__link">{attributes.link_label}</span>
      }

      return;
    }

    return (
      <div className="container">
        <div className="tile-product">
           <PlainText
            onChange={ content => setAttributes({ title: content }) }
            value={ attributes.title }
            placeholder="Your product title"
            className="tile__title"
          />
          <div class="tile__image" style={{ backgroundImage: "url(" + attributes.imageUrl + ")"}}>
            <span className="remove-image" onClick={ event => { event.preventDefault(); setAttributes({ imageUrl: '' }); } }></span>
            <MediaUpload
              onSelect={ media => { setAttributes({ imageUrl: media.url }); } }
              type="image"
              value={ attributes.imageID }
              render={ ({ open }) => getImageButton(open) }
            />
          </div>
         {renderLink()}
        </div>
        <Fragment>
          <InspectorControls>
            <PanelBody>
              <PanelRow>
                <TextControl
                  label="Product Link"
                  onChange={ content => setAttributes({ link: content }) }
                  value={ attributes.link }
                  placeholder="Product Link"
                  className={ className }
                />
              </PanelRow>
              <PanelRow>
                <TextControl
                  label="Link Label"
                  onChange={ content => setAttributes({ link_label: content }) }
                  value={ attributes.link_label }
                  placeholder="Link Label"
                  className={ className }
                />
              </PanelRow>
            </PanelBody>
          </InspectorControls>
        </Fragment>
      </div>
    );
  },

  save({ attributes }) {
    return (
      <div className="wp-block-crb-product-tile-block tile-product">
        <h4 className="tile__title">{attributes.title}</h4>
        <div className="tile__image" style={{ backgroundImage: "url(" + attributes.imageUrl + ")"}} >
        </div>

        { attributes.link && attributes.link_label && <a href={ attributes.link } className="tile__link">{attributes.link_label}</a> }
      </div>
    );
  }
});