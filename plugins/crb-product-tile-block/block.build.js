/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var _wp$editor = wp.editor,
    RichText = _wp$editor.RichText,
    MediaUpload = _wp$editor.MediaUpload,
    PlainText = _wp$editor.PlainText,
    InspectorControls = _wp$editor.InspectorControls;
var registerBlockType = wp.blocks.registerBlockType;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    TextControl = _wp$components.TextControl,
    Panel = _wp$components.Panel,
    PanelBody = _wp$components.PanelBody,
    PanelRow = _wp$components.PanelRow;
var Fragment = wp.element.Fragment;


registerBlockType('crb/product-tile', {
  title: 'CRB Product Tile Block',
  icon: 'products',
  category: 'common',
  attributes: {
    title: {
      source: 'text',
      selector: '.tile__title'
    },
    link: {
      attribute: 'href',
      selector: '.tile__link'
    },
    link_label: {
      source: 'text',
      selector: '.tile__link'
    },
    imageUrl: {
      attribute: 'src',
      selector: '.tile__image'
    }
  },

  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        className = _ref.className,
        setAttributes = _ref.setAttributes;

    var getImageButton = function getImageButton(openEvent) {
      return wp.element.createElement(
        'div',
        { className: 'button-container' },
        wp.element.createElement(
          Button,
          {
            onClick: openEvent,
            className: 'button button-large'
          },
          'Pick an image'
        )
      );
    };

    var renderLink = function renderLink() {
      {
        attributes.link;
      }
      if (attributes.link && attributes.link_label) {
        return wp.element.createElement(
          'span',
          { 'class': 'tile__link' },
          attributes.link_label
        );
      }

      return;
    };

    return wp.element.createElement(
      'div',
      { className: 'container' },
      wp.element.createElement(
        'div',
        { className: 'tile-product' },
        wp.element.createElement(PlainText, {
          onChange: function onChange(content) {
            return setAttributes({ title: content });
          },
          value: attributes.title,
          placeholder: 'Your product title',
          className: 'tile__title'
        }),
        wp.element.createElement(
          'div',
          { 'class': 'tile__image', style: { backgroundImage: "url(" + attributes.imageUrl + ")" } },
          wp.element.createElement('span', { className: 'remove-image', onClick: function onClick(event) {
              event.preventDefault();setAttributes({ imageUrl: '' });
            } }),
          wp.element.createElement(MediaUpload, {
            onSelect: function onSelect(media) {
              setAttributes({ imageUrl: media.url });
            },
            type: 'image',
            value: attributes.imageID,
            render: function render(_ref2) {
              var open = _ref2.open;
              return getImageButton(open);
            }
          })
        ),
        renderLink()
      ),
      wp.element.createElement(
        Fragment,
        null,
        wp.element.createElement(
          InspectorControls,
          null,
          wp.element.createElement(
            PanelBody,
            null,
            wp.element.createElement(
              PanelRow,
              null,
              wp.element.createElement(TextControl, {
                label: 'Product Link',
                onChange: function onChange(content) {
                  return setAttributes({ link: content });
                },
                value: attributes.link,
                placeholder: 'Product Link',
                className: className
              })
            ),
            wp.element.createElement(
              PanelRow,
              null,
              wp.element.createElement(TextControl, {
                label: 'Link Label',
                onChange: function onChange(content) {
                  return setAttributes({ link_label: content });
                },
                value: attributes.link_label,
                placeholder: 'Link Label',
                className: className
              })
            )
          )
        )
      )
    );
  },
  save: function save(_ref3) {
    var attributes = _ref3.attributes;

    return wp.element.createElement(
      'div',
      { className: 'wp-block-crb-product-tile-block tile-product' },
      wp.element.createElement(
        'h4',
        { className: 'tile__title' },
        attributes.title
      ),
      wp.element.createElement('div', { className: 'tile__image', style: { backgroundImage: "url(" + attributes.imageUrl + ")" } }),
      attributes.link && attributes.link_label && wp.element.createElement(
        'a',
        { href: attributes.link, className: 'tile__link' },
        attributes.link_label
      )
    );
  }
});

/***/ })
/******/ ]);