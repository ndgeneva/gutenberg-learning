const { registerBlockType } = wp.blocks;
const { InnerBlocks  } = wp.blockEditor;

const ALLOWED_BLOCKS = [ 'crb/feature-tile' ];

registerBlockType('crb/complex', {
  title: 'CRB Complex Block',
  icon: 'screenoptions',
  category: 'common',

  edit({ className }) {
    return (
      <div className="container">
        <InnerBlocks
          allowedBlocks={ ALLOWED_BLOCKS }
        />
      </div>
    );
  },

  save({ attributes }) {allowedBlocks={ ALLOWED_BLOCKS }
    return (
      <div className="wp-block-crb-complex-block">
        <InnerBlocks.Content />
      </div>
    );
  }
});