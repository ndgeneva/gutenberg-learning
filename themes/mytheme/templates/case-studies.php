<?php
/**
 * Template Name: Case Studies
 */

get_header();

crb_render_fragment( 'intro' );

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type'      => 'crb_case_study',
    'posts_per_page' => 3,
    'paged'          => $paged,
);

$current_case_study = 'all';
if ( isset( $_GET['filter'] ) && ! empty( $_GET['filter'] ) ) {
	$current_case_study = $_GET['filter'];
}

if ( ! empty( $current_case_study ) && $current_case_study !== 'all' ) {
	$args['tax_query'][] = array(
		'taxonomy' => 'crb_area',
		'field'    => 'slug',
		'terms'    => $current_case_study,
	);
}

$case_studies_query = new WP_Query( $args );
?>

<section class="section-cards-group section--load-more">
	<div class="section__inner">
		<?php crb_render_fragment( 'case-studies/filters' ); ?>

		<div class="section__body">
			<div class="cards">
				<?php if ( ! $case_studies_query->have_posts() ) : ?>
					<h2><?php _e( 'No posts found.', 'crb' ); ?></h2>
				<?php else : ?>
					<ul class="load-more-items">
						<?php while( $case_studies_query->have_posts() ) : $case_studies_query->the_post(); ?>
							<li data-aos="fade-up">
								<?php crb_render_fragment( 'case-studies/card', array( 'case_study_id' => get_the_ID() ) ); ?>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div><!-- /.cards -->
		</div><!-- /.section__body -->

		<?php if ( $case_studies_query->max_num_pages > 1 && $paged !== intval( $case_studies_query->max_num_pages ) ) : ?>
            <div class="section__actions" data-aos="fade-up">
                <?php carbon_pagination( 'posts', [
                    'wrapper_before' => '',
                    'wrapper_after' => '',
                    'enable_prev'  => false,
                    'current_page' => $paged,
                    'total_pages'  => $case_studies_query->max_num_pages,
                    'next_html' => '<a href="{URL}" class="btn btn--red-border btn-load-more">' . esc_html__( 'CARICA ALTRO', 'crb' ) . '</a>',
                ] );
                ?>
            </div><!-- /.section__actions -->
        <?php endif; wp_reset_postdata(); ?>
	</div><!-- /.section__inner -->
</section><!-- /.section-cards-group -->

<?php
$sections = carbon_get_the_post_meta( 'crb_sections' );

foreach ( $sections as $idx => $section ) {
	$fragment_path = 'sections/' . str_replace( '_', '-', $section['_type'] );

	crb_render_fragment( $fragment_path, [
		'section' => $section,
	] );
}

get_footer();
