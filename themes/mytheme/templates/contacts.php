<?php
/**
 * Template Name: Contacts
 */

get_header();

crb_render_fragment( 'intro' );

$locations = carbon_get_the_post_meta( 'crb_locations' );
?>

<section class="section-contacts">
	<div class="section__inner">
		<div class="contacts">
			<ul>
				<?php foreach ( $locations as $location ) : ?>
					<li data-aos="fade-up">
						<div class="contact">
							<h4><?php echo esc_html( $location['city'] ); ?></h4>

							<?php echo apply_filters( 'the_content', $location['address'] ); ?>

							<?php if ( $location['contacts'] ) : ?>
								<div class="list-contacts">
									<ul>
										<?php foreach ( $location['contacts'] as $entry ) : ?>
											<?php if ( $entry['type'] === 'email' ) : ?>
												<li>
													<a href="mailto:<?php echo antispambot( $entry['contact'] ); ?>"><span><?php _e( 'CONTATTACI VIA EMAIL', 'crb' ); ?></span></a>
												</li>
											<?php else : ?>
												<li>
													<a href="<?php echo crb_get_phone_link( $entry['contact'] ); ?>">
														<span><?php echo esc_html( $entry['contact'] ); ?></span>
													</a>
												</li>
											<?php endif ?>
										<?php endforeach ?>
									</ul>
								</div><!-- /.list-contacts -->
							<?php endif ?>
						</div><!-- /.contact -->
					</li>
				<?php endforeach ?>
			</ul>
		</div><!-- /.contacts -->
	</div><!-- /.section__inner -->
</section><!-- /.section-contacts -->

<?php get_footer(); ?>