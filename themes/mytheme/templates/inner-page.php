<?php
/**
 * Template Name: Inner Page
 */

get_header();

crb_render_fragment( 'intro' );

$sections = carbon_get_the_post_meta( 'crb_sections' );

foreach ( $sections as $idx => $section ) {
	$fragment_path = 'sections/' . str_replace( '_', '-', $section['_type'] );

	crb_render_fragment( $fragment_path, [
		'section' => $section,
	] );
}

get_footer();
