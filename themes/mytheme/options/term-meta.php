<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'term_meta', 'Area Settings' )
    ->where( 'term_taxonomy', '=', 'crb_area' )
    ->add_tab( __( 'General', 'crb' ), array(
    	Field::make( 'image', 'crb_area_intro', __( 'Intro Background', 'crb' ) )
    		->set_required( true ),
    	Field::make( 'image', 'crb_area_icon_red', __( 'Icon Red', 'crb' ) )
    		->set_required( true ),
    	Field::make( 'image', 'crb_area_icon_white', __( 'Icon White', 'crb' ) )
    		->set_required( true ),
    	Field::make( 'image', 'crb_area_thumbnail', __( 'Thumbnail', 'crb' ) )
    		->set_required( true ),
    ) )
    ->add_tab( __( 'Callout', 'crb' ), array(
    	Field::make( 'text', 'crb_area_c_content', __( 'Content', 'crb' ) ),
    	Field::make( 'text', 'crb_area_c_btn_label', __( 'Button Text', 'crb' ) ),
    	Field::make( 'text', 'crb_area_c_btn_link', __( 'Button Link', 'crb' ) ),
    	Field::make( 'checkbox', 'crb_area_c_btn_new_tab', __( 'Open in a new tab', 'crb' ) ),
    ) );